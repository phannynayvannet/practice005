import './App.css';
import NavMenu from './components/NavMenu';
import React, { Component } from 'react'
import Content from './components/Content';
import TableData from './components/TableData';

export default class App extends Component {
  constructor() {
    super()
    this.state = {
      data: [
        {
          id: 1,
          title: "Pizza",
          amount: 0,
          img: 'image/pizza.jpg',
          price: 10,
          total: 0,
        },
        {
          id: 2,
          title: "Beef (Breakfast)",
          amount: 0,
          img: "image/breakfast.jpg",
          price: 6,
          total: 0,
        },
        {
          id: 3,
          title: "Burgur",
          amount: 0, 
          img: "image/burger.jpg",
          price: 5,
          total: 0,
        },
        {
          id: 4,
          title: "Coca Cola",
          amount: 0,
          img: "image/coca.jpg",
          price: 3,
          total: 0,
        }
      ]
    }
    this.onAdd = this.onAdd.bind(this)
    this.onDelete = this.onDelete.bind(this)
  }

  onAdd(index) {
    let data = this.state.data
    data[index].amount++
    data[index].total = data[index].amount * data[index].price
    this.setState({
      data: data
    })

  }
  onDelete(index) {
    let data = this.state.data
    data[index].amount--
    data[index].total = data[index].amount * data[index].price
    this.setState({
      data: data
    })
  }

  onClear = ( ) => {
    let data = this.state.data
    data.map(item => {
      item.amount=0
      item.total=0
    })
    this.setState({
      data: data
    })
  } 

  render() {
    return (
      <div>
        <NavMenu />
        <Content
          onAdd={this.onAdd}
          onDelete={this.onDelete}
          items={this.state.data} />
        <TableData onClear={this.onClear} items={this.state.data} />
      </div>
    )
  }
}

