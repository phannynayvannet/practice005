import React from 'react'
import { Container, Row, Table, Button, Badge } from 'react-bootstrap'

function TableData({ items, onClear }) {

    let temp = items.filter(item => {
        return item.amount > 0
    })

    return (

        <Container>
            <Row className="count">
                <Button
                    variant="danger"
                    className="mx-2"
                    onClick={onClear}
                >Clear all</Button>
                <Badge variant="warning">{temp.length} Count</Badge>

            </Row>
            <Row>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Food</th>
                            <th>Amount</th>
                            <th>Price</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        {temp.map((item, idx) => (
                            <tr>
                                <td>{idx + 1}</td>
                                <td>{item.title}</td>
                                <td>{item.amount}</td>
                                <td>{item.price}</td>
                                <td>{item.total}</td>
                            </tr>
                        ))}
                    </tbody>
                </Table>

            </Row>
        </Container>
    )
}

export default TableData
